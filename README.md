netty-server
============

项目介绍:介绍Netty的基本使用方法
---------------
        开发环境: IntelliJ IDEA+ JDK1.7
        




#文档
Maven
-----
http://mvnrepository.com/<br/>
http://search.maven.org/
Spring
------
http://projects.spring.io/spring-framework/<br/>
http://docs.spring.io/spring/docs/4.0.5.RELEASE/spring-framework-reference/html/spring-web.html
Mybatis
-------
http://mybatis.github.io/spring/zh/index.html
FreeMarker
---------
http://freemarker.org/
Druid
-----
https://github.com/alibaba/druid
Quartz
------
http://quartz-scheduler.org/documentation
Ehcache
-------
http://ehcache.org/documentation/get-started
OsCache
-------
https://java.net/downloads/oscache/
MapDB
-----
http://www.mapdb.org/doc/cheatsheet.pdf
http://www.mapdb.org/02-getting-started.html
Logback
-------
http://logback.qos.ch/manual/index.html

模仿Oauth2.0 协议
-------
模仿Oauth2.0 协议，完成对获得 code、 获得 access_token, refresh_token 使用的示例

1、基于Netty构建的Restful服务架构，实现rest接口服务
2、学习Netty的基本使用方法，为下一步做网络通信做好基础服务
