package com.xiaoshu;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslContext;

/**
 * 
 * 功能说明：
 * 
 * ServerInitializer.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月1日上午10:52:24
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
public class ServerInitializer  extends ChannelInitializer<SocketChannel>{

	  private final SslContext sslCtx;

	    public ServerInitializer(SslContext sslCtx) {
	        this.sslCtx = sslCtx;
	    }

	    @Override
	    public void initChannel(SocketChannel ch) {
	        ChannelPipeline p = ch.pipeline();
	        if (sslCtx != null) {
	            p.addLast(sslCtx.newHandler(ch.alloc()));
	        }
	        p.addLast(new HttpServerCodec());/*HTTP 服务的解码器*/
	        p.addLast(new HttpObjectAggregator(2048));/*HTTP 消息的合并处理*/
	        p.addLast(new HealthServerHandler()); /*自己写的服务器逻辑处理*/
	    }
	    
	    @Override
	    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception{
	    	System.err.println("Failed to initialize a channel. Closing: " + ctx.channel() +"cause :" + cause);
	        ctx.close();
	    }
}
