package com.xiaoshu.test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ResponseServerHandler extends ChannelInboundHandlerAdapter{

	   @Override
	    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
		   ctx.write(msg);
	   }
	   
	    @Override
	    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
	    	ctx.flush();
	    }
	    
	    @Override
	    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	            throws Exception{
	    	System.out.println("exception is cause :" + cause.getMessage());
	    }
}
