package com.xiaoshu.test;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;

public class DiscardServerHandler extends ChannelInboundHandlerAdapter{

	
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    	 try {
	             ByteBuf in = (ByteBuf) msg;
	           /*  while (in.isReadable()) {
	                 System.out.print((char) in.readByte());
	                 System.out.flush();
	             }*/
	             //这一句和上面注释的的效果都是打印输入的字符
	             System.out.println(in.toString(CharsetUtil.US_ASCII));
	         }finally {
	             /**
	              * ByteBuf是一个引用计数对象，这个对象必须显示地调用release()方法来释放。
	              * 请记住处理器的职责是释放所有传递到处理器的引用计数对象。
	              */
	             ReferenceCountUtil.release(msg);
	         }
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause){
    	System.out.println("logger the exception is :" + cause.getMessage());
    }
}
