package com.xiaoshu;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import org.json.JSONObject;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.util.AsciiString;
import io.netty.util.CharsetUtil;

/**
 * 
 * 功能说明：
 * 
 * TestHealthServerHandler.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月1日上午10:52:12
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
public class TestHealthServerHandler extends ChannelInboundHandlerAdapter {

	 private static final AsciiString CONTENT_TYPE = new AsciiString("Content-Type");
	 private static final AsciiString CONTENT_LENGTH = new AsciiString("Content-Length");
	 private static final AsciiString CONNECTION = new AsciiString("Connection");
	 private static final AsciiString KEEP_ALIVE = new AsciiString("keep-alive");
	
	 @Override
	 public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		 ctx.flush();
	 }
	 
	 @Override
	 public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		 if (msg instanceof FullHttpRequest) {
	            FullHttpRequest req = (FullHttpRequest) msg;//客户端的请求对象
	            JSONObject responseJson = new JSONObject();//新建一个返回消息的Json对象

	            //把客户端的请求数据格式化为Json对象
	            JSONObject requestJson = null;
	            try{
	               requestJson = new JSONObject(parseJosnRequest(req));
	            }catch(Exception e)
	            {
	                ResponseJson(ctx,req,new String("error json"));
	                return;
	            }

	            //String uri = req.uri();//获取客户端的URL

	            //根据不同的请求API做不同的处理(路由分发)，只处理POST方法
	            if (req.method() == HttpMethod.POST) {
	                if(req.uri().equals("/oauth2/token"))
	                { 
	                    //计算体重质量指数
	                    String grant_type = requestJson.getString("grant_type");
	                    if(grant_type.equals("authorization_code")){
	                    	responseJson.put("access_token", "6641028bb7fcf98c738fd84b5acdd07f");
	                    	responseJson.put("refresh_token", "eebf2d30f090f3052ea8be0745c4964e");
	                    	responseJson.put("expires_in", 7200);
	                    	responseJson.put("token_type", "bearer");
	                    }
	                }else {
	                    //错误处理
	                    responseJson.put("error", "404 Not Find");
	                }

	            } else {
	                //错误处理
	                responseJson.put("error", "404 Not Find");
	            }

	            //向客户端发送结果
	            ResponseJson(ctx,req,responseJson.toString());
	        }
	 }
	 
	 
	 @Override
	 public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	            throws Exception {
		 System.out.println("cause is :" + cause.getMessage());
	 }
	 
	   /**
	     * 获取请求的内容
	     * @param request
	     * @return
	     */
	    private String parseJosnRequest(FullHttpRequest request) {
	        ByteBuf jsonBuf = request.content();
	        String jsonStr = jsonBuf.toString(CharsetUtil.UTF_8);
	        return jsonStr;
	    }
	    
	    /**
	     * 响应HTTP的请求
	     * @param ctx
	     * @param req
	     * @param jsonStr
	     */
	    private void ResponseJson(ChannelHandlerContext ctx, FullHttpRequest req ,String jsonStr)
	    {

	        boolean keepAlive = HttpUtil.isKeepAlive(req);
	        byte[] jsonByteByte = jsonStr.getBytes();
	        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(jsonByteByte));
	        response.headers().set(CONTENT_TYPE, "text/json");
	        response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes());

	        if (!keepAlive) {
	            ctx.write(response).addListener(ChannelFutureListener.CLOSE);
	        } else {
	            response.headers().set(CONNECTION, KEEP_ALIVE);
	            ctx.write(response);
	        }
	    }
}
